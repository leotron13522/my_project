from flask import Flask, render_template, request
from GameLife import GameOfLife


app = Flask(__name__)


@app.route('/', methods=['post', 'get'])
def index():
    width = 10
    height = 10
    if request.method == "POST":
        width = int(request.form.get('width'))
        height = int(request.form.get('height'))
    GameOfLife(width, height)
    return render_template('index.html', width=width, height=height)


@app.route('/live')
def live():
    life = GameOfLife()
    if life.cycle_counter > 0:
        life.form_new_generation()
    life.cycle_counter = life.cycle_counter + 1
    return render_template('live.html', life=life)


if __name__ == '__main__':
    app.run(port=3030)
