import socket

host = socket.gethostbyname(socket.gethostname())
server = host, 9090
alias = input("Name: ")
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.sendto(('[{}] Connect to server'.format(alias)).encode('utf-8'), server)
client_work = True

while client_work:
    try:
        message = input("Message:")
        s.sendto(('[{}] {}'.format(alias, message)).encode('utf-8'), server)
    except Exception:
        s.sendto(('{} disconnect from server'.format(alias)).encode('utf-8'), server)
        client_work = False
