import socket
import mysql.connector
from mysql.connector import Error
import traceback


def connect_to_db():
    try:
        conn = mysql.connector.connect(host='localhost',
                                       database='db_for_client_server',
                                       user='****',
                                       password='******')
        if conn.is_connected():
            print('Connected to MySQL database')
            return conn
        else:
            raise Exception("ConnectionError")
    except Error as e:
        print('Connection to MySQL failed.', e)
        raise e


def execute_write_query(conn, query, args=None):
    cursor = conn.cursor()
    cursor.execute(query, args)
    conn.commit()
    results = "Message №:", cursor.lastrowid, " add."
    print(results)
    cursor.close()


def create_server():
    host = socket.gethostbyname(socket.gethostname())
    port = 9090
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server.bind((host, port))
    print("Server started at: ", host)
    return server


def message_get(conn, server):
    server_work = True
    while server_work:
        try:
            data, addres = server.recvfrom(1024)
            data = data.decode("utf-8")
            message = """INSERT INTO clients (name, message, date_time) 
                    VALUES (%s, %s, NOW());"""
            delim = data.find(' ')
            args = (data[:delim], data[delim+1:])
            print(args)
            execute_write_query(conn, message, args)
        except Exception:
            print("\nServer Stop", traceback.format_exc())
            server_work = False
    server.close()
    conn.close()


def create_db_table(conn):
    create_table = """
    CREATE TABLE IF NOT EXISTS clients (
        client_id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(40),
        message VARCHAR(200),
        date_time DATETIME
    );
    """
    execute_write_query(conn, create_table)
    table_static_inf = """
    INSERT INTO clients (name, message, date_time) 
        VALUES (%s, %s, NOW());
    """
    table_static_inf_args = ("server", "server start")
    execute_write_query(conn, table_static_inf, table_static_inf_args)


def main():
    server = create_server()
    conn = connect_to_db()
    create_db_table(conn)
    message_get(conn, server)


if __name__ == '__main__':
    main()
