import socket
import mysql.connector
from mysql.connector import Error as MySqlError
import threading

END_OF_MESSAGE = "\n\n"
DELIMITER = '\n'


def connecting_to_db():
    try:
        connect_to_db = mysql.connector.connect(host='localhost',
                                                database='db_for_client_server',
                                                user='*****',
                                                password='********')
    except MySqlError as db_error:
        print(db_error)
        raise db_error

    if connect_to_db.is_connected():
        print('Connected to MySQL database')
        return connect_to_db
    else:
        raise Exception("ConnectionError")


def execute_write_query(connect_to_db, query, args=None):
    with connect_to_db.cursor() as cursor:
        cursor.execute(query, args)
        connect_to_db.commit()


def execute_read_query(connect_to_db, query):
    with connect_to_db.cursor() as cursor:
        cursor.execute(query)
        information_from_db = cursor.fetchall()
        return information_from_db


def create_db_table(db_connection):
    try:
        create_table = """
        CREATE TABLE IF NOT EXISTS metrics (
            mes VARCHAR(40),
            timestamp VARCHAR(200),
            value VARCHAR(200),
            UNIQUE (mes, timestamp)
            );
        """
        execute_write_query(db_connection, create_table)
        print("Table metrics created")
    except Exception as db_creation_exception:
        print("Table metrics creating failed.", db_creation_exception)


def get_message(db_connection, message, server_connection):
    query = """
        SELECT mes, value, timestamp FROM metrics;
        """
    key = message[1]
    if key != '*':
        query = """
        SELECT mes, value, timestamp FROM metrics WHERE (mes = "{}")
        ORDER BY value;
        """.format(key)
    answer = execute_read_query(db_connection, query)
    if not answer:
        return_message = "ok" + END_OF_MESSAGE
    else:
        return_message = "ok" + DELIMITER + DELIMITER.join(map(' '.join, answer)) + END_OF_MESSAGE
    server_connection.send(return_message.encode("utf8"))


def process_request(server_connection, server_adress, db_connection):
    print("Client connected on: ", server_adress)
    with server_connection:
        while True:
            data = server_connection.recv(1024)
            message = data.decode("utf8").split()
            if not data or not message:
                break
            data_start = message[0]
            if data_start == "put" and 3 <= len(message) <= 4:
                put_message(db_connection, message, server_connection)
            elif data_start == "get" and len(message) == 2:
                get_message(db_connection, message, server_connection)
            else:
                server_connection.sendall(b"error\nwrong command\n\n")


def run_server(host, port, db_connection):
    with socket.socket() as sock:
        sock.bind((host, port))
        sock.listen()
        print("Server connection created")
        while True:
            server_connection, server_adress = sock.accept()
            th = threading.Thread(target=process_request,
                                  args=(server_connection, server_adress, db_connection))
            th.start()


def put_message(db_connection, message, server_connection):
    if 3 <= len(message) <= 4:
        key, value, timestamp = message[1], message[2], message[3]
    else:
        print("Wrong input values")
        raise Exception()
    server_connection.sendall(b"ok\n\n")
    try:
        table_static_inf = """
            INSERT INTO metrics (mes, timestamp, value)
                VALUES (%s, %s, %s);
            """
        table_static_inf_args = (key, timestamp, value)
        execute_write_query(db_connection, table_static_inf, table_static_inf_args)
    except mysql.connector.errors.IntegrityError:
        table_static_inf = """
            UPDATE metrics SET value = (%s)
            WHERE mes = (%s) AND timestamp = (%s);
            """
        table_static_inf_args = (value, key, timestamp)
        execute_write_query(db_connection, table_static_inf, table_static_inf_args)


def main():
    db_connection = connecting_to_db()
    create_db_table(db_connection)
    run_server("127.0.0.1", 8888, db_connection)


if __name__ == '__main__':
    main()
