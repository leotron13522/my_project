import time
import socket

END_OF_MESSAGE = "\n\n"
DELIMITER = '\n'


class Client:

    def __init__(self, host, port, timeout=None):
        self.host = host
        self.port = port
        self.timeout = timeout

    def __enter__(self):
        self.connection = self.connect_to_server(self.host, self.port, self.timeout)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()
        print('connection closed')

    def connect_to_server(self, host, port, timeout):
        print("connected to: ", host, " on port: ", port)
        server_connection = socket.socket()
        server_connection.settimeout(timeout)
        server_connection.connect((host, port))
        return server_connection

    def get_data_from_server(self, metric_key):
        try:
            self.connection.send('get {}\n'.format(metric_key).encode('utf-8'))
            data_answer = ""
            while True:
                received_data = self.connection.recv(1024)
                data_answer = data_answer + received_data.decode('utf-8')
                if data_answer.endswith(END_OF_MESSAGE):
                    returning_answer = self.make_answer_dict(data_answer)
                    return returning_answer
                break
        except Exception:
            print(ClientError.message)
            raise ClientError()

    def put_data_to_server(self, metric_key, metric_value, timestamp):
        self.connection.send('put {} {} {}\n'.format(metric_key,
                                                     metric_value,
                                                     timestamp).encode('utf-8'))
        if self.connection.recv(1024) != b'ok\n\n':
            print(ClientError.message)
            raise ClientError()
        else:
            print("Data send successfully")

    def make_answer_dict(self, answer):
        work_with_answer = answer.split(DELIMITER)
        answer_dict = {}
        for part in work_with_answer[1:]:
            if len(part) != 0:
                key, value, timestamp = part.split()
                if key not in answer_dict:
                    answer_dict[key] = [(timestamp, value)]
                else:
                    answer_dict[key] += [(timestamp, value)]
        return answer_dict

    def input_message_rules(self):
        print("If you want add metrics to storage, please, write: put key value timestamp (if need)")
        print("If you want take metrics from storage, please, write: get * (all metrics) or key-name")
        print("If you want to exit, write: exit")

    def start_client_work(self):
        client_work = True
        self.input_message_rules()
        while client_work:
            try:
                message = input("Message:")
                if message == "exit":
                    print("Bye!")
                    client_work = False
                else:
                    message = message.split()
                    if message[0] == "put" and 3 <= len(message) <= 4:
                        if len(message) == 3:
                            key, value, timestamp = message[1], message[2], time.time()
                        else:
                            key, value, timestamp = message[1], message[2], message[3]
                        client.put_data_to_server(key, value, timestamp)
                    elif message[0] == "get" and len(message) == 2:
                        print(client.get_data_from_server(message[1]))
                    else:
                        print("Sorry, wrong message!")
                        self.input_message_rules()
            except Exception as e:
                print("Catch error: ", e)
                client_work = False


class ClientError(Exception):
    message = "There was an Error while sending/receiving data to/from server."


with Client("127.0.0.1", 8888) as client:
    client.start_client_work()
